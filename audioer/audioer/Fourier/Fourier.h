#pragma once

#define PI 3.1415926535897932384626433832795
#define HALF_PI 1.5707963267948966192313216916398
#define TWO_PI 6.283185307179586476925286766559

struct Signal
{
	double real;
	double im;
	long freq;

	Signal& operator*=(double& x);
	Signal& operator*(double& x);

	Signal& operator*=(const Signal& other);
	Signal& operator*(const Signal& other);


	Signal& operator+=(const Signal& other);
	Signal& operator+(const Signal& other);

	void kill();

	double amp();
	double phase();
};

void dft(double* input, Signal* output, long N);
void idft(Signal* input, double* output, long N);
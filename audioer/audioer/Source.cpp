#include <iostream>


#include "AudioFile/AudioFile.h"
#include "Fourier/Fourier.h"

#define INPUT_FILE "מה תם עושים.wav"
#define OUTPUT_FILE "output2.wav"

#define BUFFER_LEN 1024

void kill_noise(double* input, size_t length)
{
	std::vector<Signal> signals(length);
	dft(input, signals.data(), length);

	for (auto& signal : signals) {
		if (signal.amp() >= 1e-3) {
			signal.kill();
		}
	}

	idft(signals.data(), input, length);
}

void transform(double* input, size_t length, size_t channels)
{
	double channels_arr[10][BUFFER_LEN] = { 0 };

	for (int chan = 0; chan < channels; chan++)
		for (int i = 0; i < length / channels; i++)
			channels_arr[chan][i] = input[chan + (i * channels)];

	for (int i = 0; i < channels; i++) {
		kill_noise(channels_arr[i], length / channels);
	}
	
	for (int chan = 0; chan < channels; chan++)
		for (int i = 0; i < length / channels; i++)
			input[chan + (i * channels)] = channels_arr[chan][i];
}

int main()
{

	try {
		AudioFile input_file;
		input_file.open(INPUT_FILE, SFM_READ);

		AudioFile output_file;
		output_file.file_info = input_file.file_info;
		output_file.open(OUTPUT_FILE, SFM_WRITE);
	
		double arr[BUFFER_LEN] = { 0 };
		size_t bytes_read = 0;

		while(bytes_read = input_file.read(arr, BUFFER_LEN)){
			
			transform(arr, bytes_read, output_file.file_info.channels);
			output_file.write(arr, bytes_read);
		}

	}
	catch (std::exception& e) {
		std::cout << e.what();
	}
	return 0;
}
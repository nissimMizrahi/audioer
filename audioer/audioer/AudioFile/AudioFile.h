#pragma once

#include <string>
#include <memory>
#include <vector>

#include <sndfile.h>

#include "SndFilePtr/SndFilePtr.h"

class AudioFile
{
public:


	AudioFile();
	
	void open(const std::string& file_path, int permissions);

	size_t read(double* out_buffer, size_t length);
	size_t write(double* in_buffer, size_t length);

	SF_INFO file_info;

private:

	SndFilePtr m_file_handle;

};


#include <stdexcept>
#include <sndfile.h>

class SndFileExcption : public std::exception
{
public:
	SndFileExcption(SNDFILE* handle): std::exception(sf_strerror(handle)) {}
};
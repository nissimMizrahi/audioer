#include "AudioFile.h"
#include <iostream>

#include "exceptions/SndFileException.h"

AudioFile::AudioFile() : m_file_handle(nullptr), file_info({0})
{	
}

void AudioFile::open(const std::string& file_path, int permissions)
{
	auto file_ptr = sf_open(file_path.c_str(), permissions, &file_info);
	m_file_handle = file_ptr;

	if (m_file_handle.get() == nullptr) {
		throw SndFileExcption(file_ptr);
	};
}

size_t AudioFile::read(double* out_buffer, size_t length)
{
	return sf_read_double((SNDFILE*)m_file_handle.get(), out_buffer, length);
}

size_t AudioFile::write(double* in_buffer, size_t length)
{
	return sf_write_double((SNDFILE*)m_file_handle.get(), in_buffer, length);
}


#include "SndFilePtr.h"
#include "..//exceptions/SndFileException.h"


SndFilePtr::SndFilePtr(SNDFILE* file_handle) : m_file_handle(file_handle)
{

}

SndFilePtr::SndFilePtr(SndFilePtr&& other)
{
	*this = std::move(other);
}

SndFilePtr& SndFilePtr::operator=(SndFilePtr&& other)
{
	auto tmp_file_handle = other.m_file_handle;
	other.m_file_handle = nullptr;
	this->m_file_handle = tmp_file_handle;

	return *this;
}

SNDFILE const* SndFilePtr::get()
{
	return m_file_handle;
}

SndFilePtr::~SndFilePtr()
{
	try {
		handle_dying();
	}catch(...) {}
}

void SndFilePtr::handle_dying()
{
	if (nullptr != m_file_handle) {
		if (sf_close(m_file_handle)) {
			SndFileExcption exception(m_file_handle);
			m_file_handle = nullptr;
			throw exception;
		}
		m_file_handle = nullptr;
	}
}

#pragma once

#include <sndfile.h>

class SndFilePtr
{
public:

	SndFilePtr(SNDFILE* file_handle);

	SndFilePtr(const SndFilePtr& other) = delete;
	SndFilePtr& operator=(const SndFilePtr& other) = delete;

	SndFilePtr(SndFilePtr&& other);
	SndFilePtr& operator=(SndFilePtr&& other);

	const SNDFILE const* get();

	~SndFilePtr();

private:

	void handle_dying();

	SNDFILE* m_file_handle;

};

